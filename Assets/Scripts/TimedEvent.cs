﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace JGJK
{
    public class TimedEvent : MonoBehaviour
    {
        public UnityEvent timedOut;
        public float duration = 5;
        public bool autoStart = true;
        public bool destroyOnTimeout = true;

        private void Start()
        {
            if (autoStart)
            {
                StartTimer();
            }
        }

        public void StartTimer()
        {
            StartCoroutine(TriggerAfterTimeout());
        }

        private IEnumerator TriggerAfterTimeout()
        {
            yield return new WaitForSeconds(duration);
            timedOut?.Invoke();
            if (destroyOnTimeout)
            {
                Destroy(this);
            }
        }
    }
}