﻿using UnityEngine;

namespace JGJK
{
    public class HideWhenFullscreen : MonoBehaviour
    {
        [SerializeField] private CanvasGroup canvasGroup;

        private float normalAlpha;

        private void Start()
        {
            normalAlpha = canvasGroup.alpha;
            UpdateVisibility();
        }

        // Don't really need to do this every frame, but whatever
        private void Update() => UpdateVisibility();

        private void UpdateVisibility()
            => canvasGroup.alpha = Screen.fullScreen ? 0 : normalAlpha;
    }
}