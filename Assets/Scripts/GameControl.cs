﻿using MessagePipe;
using NaughtyAttributes;
using UnityEngine;
using Zenject;

public class GameControl : MonoBehaviour
{
    [Inject] GameTime gameTime;
    [Inject] IPublisher<PauseStateChanged> pauseEvents;
    [Inject] IPublisher<WinEvent> winEvents;
    [Inject] IPublisher<LoseEvent> loseEvents;
    [ShowNonSerializedField] bool won;
    [ShowNonSerializedField] bool lost;
    [ShowNativeProperty] public bool Paused { get; private set; } = true;

    [Button] public void Pause()
    {
        if (won || lost) return;
        if (Paused) return;
        gameTime.Pause();
        Paused = true;
        pauseEvents.Publish(new PauseStateChanged { Paused = true });
    }

    [Button] public void Resume()
    {
        if (won || lost) return;
        if (!Paused) return;
        gameTime.Unpause();
        Paused = false;
        pauseEvents.Publish(new PauseStateChanged { Paused = false });
    }

    [Button]
    public void Win()
    {
        if (won || lost) return;
        won = true;
        winEvents.Publish(new WinEvent());
    }

    [Button]
    public void Lose()
    {
        if (won || lost) return;
        lost = true;
        loseEvents.Publish(new LoseEvent());
    }
}

public struct WinEvent { }
public struct LoseEvent { }

public struct PauseStateChanged
{
    public bool Paused { get; set; }
}
