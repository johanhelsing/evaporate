using NaughtyAttributes;
using UnityEngine;

public class HissingVolume : MonoBehaviour
{
    [Required] [SerializeField] SunExposure exposure;
    [Required] [SerializeField] AudioSource hissingSoundSource;

    void Update() => hissingSoundSource.volume = exposure.Exposure;
}
