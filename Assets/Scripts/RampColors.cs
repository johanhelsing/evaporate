using NaughtyAttributes;
using UnityEngine;

[DisallowMultipleComponent]
public class RampColors : MonoBehaviour
{
    [SerializeField] Color shadow = Color.black;
    [SerializeField] Color dark = Color.blue;
    [SerializeField] Color mid = Color.cyan;
    [SerializeField] Color highlight = Color.white;

    [Range(0,1)] [SerializeField] float darkThreshold = 0.2f;
    [Range(0,1)] [SerializeField] float midThreshold = 0.5f;
    [Range(0,1)] [SerializeField] float highlightThreshold = 1f;

    [SerializeField] float max = 1.5f;
    [SerializeField] float min = 0f;

    [Required][SerializeField] new Renderer renderer;

    static readonly int ShadowId = Shader.PropertyToID("_Shadow");
    static readonly int DarkId = Shader.PropertyToID("_Dark");
    static readonly int MidId = Shader.PropertyToID("_Mid");
    static readonly int HighlightId = Shader.PropertyToID("_Highlight");

    static readonly int MaxId = Shader.PropertyToID("_Max");
    static readonly int MinId = Shader.PropertyToID("_Min");

    static readonly int DarkThresholdId = Shader.PropertyToID("_DarkThreshold");
    static readonly int MidThresholdId = Shader.PropertyToID("_MidThreshold");
    static readonly int HighlightThresholdId = Shader.PropertyToID("_HighlightThreshold");

    static MaterialPropertyBlock block;

    void Awake() => UpdateProperties();

    void OnValidate()
    {
        if (renderer == null)
        {
            renderer = GetComponent<Renderer>();
        }
        UpdateProperties();
    }

    void UpdateProperties()
    {
        block ??= new MaterialPropertyBlock();
        block.SetColor(ShadowId, shadow);
        block.SetColor(DarkId, dark);
        block.SetColor(MidId, mid);
        block.SetColor(HighlightId, highlight);

        block.SetFloat(MaxId, max);
        block.SetFloat(MinId, min);

        block.SetFloat(DarkThresholdId, darkThreshold);
        block.SetFloat(MidThresholdId, midThreshold);
        block.SetFloat(HighlightThresholdId, highlightThreshold);

        renderer.SetPropertyBlock(block);
    }
}
