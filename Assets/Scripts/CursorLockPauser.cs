﻿using System;
using MessagePipe;
using UnityEngine;

public class CursorLockPauser : IDisposable
{
    readonly CompositeDisposable disposables = new CompositeDisposable();
    readonly GameControl gameControl;

    public CursorLockPauser(GameControl gameControl, ISubscriber<CursorLockMode> lockEvents)
    {
        lockEvents
            .Subscribe(mode =>
            {
                if (mode == CursorLockMode.Locked)
                {
                    gameControl.Resume();
                }
                else
                {
                    gameControl.Pause();
                }
            })
            .AddTo(disposables);
    }

    public void Dispose()
    {
        disposables.Dispose();
    }
}
