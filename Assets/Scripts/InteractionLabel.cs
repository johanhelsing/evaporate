using MessagePipe;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using Zenject;

public class InteractionLabel : MonoBehaviour
{
    [Required] [SerializeField] TextMeshProUGUI text;

    [Inject] ISubscriber<Interactable> currentInteractable;

    Interactable interactable;

    void Start()
    {
        text.text = "";
        currentInteractable.Subscribe(interactable =>
        {
            this.interactable = interactable;
            text.text = "";
        });
    }

    void Update()
    {
        if (interactable != null)
        {
            var verb = interactable.Verb;

            if (string.IsNullOrEmpty(verb))
            {
                verb = "Interact";
            }

            text.text = verb;
        }
    }

    void OnValidate()
    {
        if (text != null)
        {
            text = GetComponent<TextMeshProUGUI>();
        }
    }
}
