using NaughtyAttributes;
using Unity.Mathematics;
using UnityEngine;

public class MeltAnimation : MonoBehaviour
{
    [Required] [SerializeField] Animator animator;
    [Required] [SerializeField] Health health;

    void Start()
    {
        animator.speed = 0;
    }

    void Update()
    {
        var normalizedTime = math.clamp(1 - health.Hp, 0.01f, 0.99f);
        animator.Play("SnowmanMelt", 0, normalizedTime);
    }
}
