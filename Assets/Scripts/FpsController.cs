using JGJK;
using NaughtyAttributes;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

public class FpsController : MonoBehaviour
{
    [Required] [SerializeField] CharacterController controller;
    [FormerlySerializedAs("camera")] [Required] [SerializeField] Transform cam;
    [Range(0, 10)] [SerializeField] public float mouseHorizontalSpeed = 2f;
    [Range(0, 10)] [SerializeField] public float mouseVerticalSpeed = 2f;
    [Min(0)] [SerializeField] public float moveSpeed = 3f;
    [SerializeField] float jumpHeight = 1f;
    [SerializeField] float gravity = 20; // <-- replace with time to peak?
    [SerializeField] float webGLSensitivityCorrection = 0.5f;

    [Inject] ICursorLockState cursor;

    // Unity doesn't seem to care about platform consistency:
    // https://forum.unity.com/threads/mouse-sensitivity-in-webgl-way-too-sensitive.411574/
#if UNITY_WEBGL && !UNITY_EDITOR
    float MouseSensitivityCorrection => webGLSensitivityCorrection;
#else
    float MouseSensitivityCorrection => 1;
#endif

    void MouseRotate()
    {
        // Pitch
        var vertical = -Input.GetAxis("Mouse Y") * mouseVerticalSpeed * MouseSensitivityCorrection;
        // camera.Rotate(vertical, 0, 0, Space.Self); // This causes drifting sometimes
        var oldRotation = cam.localRotation.eulerAngles.x;
        cam.localRotation = Quaternion.Euler(oldRotation + vertical, 0, 0);

        // Yaw
        var horizontal = Input.GetAxis("Mouse X") * mouseHorizontalSpeed * MouseSensitivityCorrection;
        transform.Rotate(0, horizontal, 0, Space.World);
    }

    void LimitPitch()
    {
        var right = cam.right;
        var angle = Vector3.SignedAngle(cam.up, Vector3.up, right);
        var clamped = Mathf.Clamp(angle, -90, 90);
        var error = angle - clamped;
        cam.Rotate(right, error);
    }

    Vector3 vel;

    void Update()
    {
        if (!cursor.Locked) return;

        // clear horizontal movement
        vel = vel._y_();

        if (!controller.isGrounded)
        {
            vel.y -= gravity * Time.deltaTime;
        }

        MouseRotate();
        LimitPitch();
        var inputDirection = InputExtensions.Direction();
        vel += transform.TransformVector(inputDirection.x_y()) * moveSpeed;

        if (controller.isGrounded && Input.GetButtonDown("Jump"))
        {
            // 0.5 * m * v * v = m * g * h
            vel.y = math.sqrt(2 * gravity * jumpHeight);
        }

        controller.Move(vel * Time.deltaTime);
    }

    void OnValidate()
    {
        if (controller == null)
        {
            controller = GetComponent<CharacterController>();
        }
    }
}
