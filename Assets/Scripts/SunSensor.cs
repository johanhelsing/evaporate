using UnityEngine;
using Zenject;

public class SunSensor : MonoBehaviour
{
    [Inject] Sun sun;

    [SerializeField] bool shaded;
    [SerializeField] LayerMask mask;

    public bool Shaded => shaded;

    void Update()
    {
        shaded = Physics.Raycast(transform.position, -sun.Direction, Mathf.Infinity, mask);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = shaded ? Color.red : Color.green;
        Gizmos.DrawSphere(transform.position, 0.05f);
        if (sun != null)
        {
            Gizmos.DrawLine(transform.position, transform.position - sun.Direction * 0.5f);
        }
    }
}
