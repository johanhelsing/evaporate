using NaughtyAttributes;
using UnityEngine;

public class Sweating : MonoBehaviour
{
    [Required] [SerializeField] ParticleSystem sweatParticles;
    [Required] [SerializeField] SunExposure exposure;

    float fullRate;

    void Awake()
    {
        fullRate = sweatParticles.emission.rateOverTimeMultiplier;
    }

    void Update()
    {
        var emission = sweatParticles.emission;
        emission.rateOverTimeMultiplier = exposure.Exposure * fullRate;
    }
}
