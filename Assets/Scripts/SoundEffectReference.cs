using System.Collections;
using System.Collections.Generic;
using MessagePipe;
using UnityEngine;
using Zenject;

public class SoundEffectReference : MonoBehaviour
{
    [SerializeField] ScriptableSoundEffect effect;
    [Inject] IPublisher<ISoundEffect> publisher;
    public void Play() => publisher.Publish(effect);
}
