﻿#if UNITY_WEBGL
#if !UNITY_EDITOR
using System.Runtime.InteropServices;
#endif
using UnityEngine;

namespace JGJK
{
    public class JohanWebGL : MonoBehaviour
    {
#if UNITY_EDITOR
        public static int AddOpenUrlInNewTabClickHandler(string url)
        {
            Debug.LogWarning("JavaScript doesn't work in the editor");
            return 0;
        }
#else
        [DllImport("__Internal")]
        public static extern int AddOpenUrlInNewTabClickHandler(string url);
#endif

#if UNITY_EDITOR
        public static int AddFullscreenClickHandler()
        {
            Debug.LogWarning("JavaScript doesn't work in the editor");
            return 0;
        }
#else
        [DllImport("__Internal")]
        public static extern int AddFullscreenClickHandler();
#endif

#if UNITY_EDITOR
        public static void RemoveFullscreenClickHandler(int handler)
            => Debug.LogWarning("JavaScript doesn't work in the editor");
#else
        [DllImport("__Internal")]
        public static extern void RemoveFullscreenClickHandler(int handler);
#endif

#if UNITY_EDITOR
        public static int AddCursorLockClickHandler()
        {
            Debug.LogWarning("AddCursorLockClickHandler: JavaScript doesn't work in the editor");
            return 0;
        }
#else
        [DllImport("__Internal")]
        public static extern int AddCursorLockClickHandler();
#endif

#if UNITY_EDITOR
        public static void RemoveCursorLockClickHandler(int handler)
            => Debug.LogWarning("RemoveCursorLockClickHandler: JavaScript doesn't work in the editor");
#else
        [DllImport("__Internal")]
        public static extern void RemoveCursorLockClickHandler(int handler);
#endif

#if UNITY_EDITOR
        public static void InstallCursorLockDetector()
            => Debug.LogWarning("InstallCursorLockDetector: JavaScript doesn't work in the editor");
#else
        [DllImport("__Internal")]
        public static extern void InstallCursorLockDetector();
#endif

#if UNITY_EDITOR
        public static void UnlockCursor()
            => Debug.LogWarning("UnlockCursor: JavaScript doesn't work in the editor");
#else
        [DllImport("__Internal")]
        public static extern void UnlockCursor();
#endif

#if UNITY_EDITOR
        public static bool InIframe()
        {
            // Debug.LogWarning("JavaScript doesn't work in the editor");
            return false;
        }
#else
        [DllImport("__Internal")]
        public static extern bool InIframe();
#endif

#if UNITY_EDITOR
        public static string DocumentReferrer() => "UnityEditor.Referrer";
#else
        [DllImport("__Internal")]
        public static extern string DocumentReferrer();
#endif
    }
}
#endif // UNITY_WEBGL