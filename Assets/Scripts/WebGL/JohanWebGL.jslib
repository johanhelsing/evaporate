const JohanWebGL = {

    $Data: {
        nextId: 0,
        handlers: {},
        pendingCursorLock: 1
    },

    AddOpenUrlInNewTabClickHandler: function (urlPointer) {
        const url = Pointer_stringify(urlPointer);
        const id = Data.nextId++;
        console.log("We should now try to open the url if the user clicks", id);
    },

    AddFullscreenClickHandler: function () {
        const id = Data.nextId++;
        Data.handlers[id] = function() {
            console.log("Trying to enter fullscreen", id);
            const instance = window.unityInstance || window.gameInstance;
            instance.SetFullscreen(1);
            document.removeEventListener("mouseup", Data.handlers[id]);
            delete Data.handlers[id];
        };
        document.addEventListener("mouseup", Data.handlers[id]);
        console.log("Installed fullscreen handler", id);
        return id;
    },

    RemoveFullscreenClickHandler: function (id) {
        document.removeEventListener("mouseup", Data.handlers[id]);
        delete Data.handlers[id];
        console.log("removed handler", id);
    },

    // Could probably try to share code with fullscreen handler above...
    AddCursorLockClickHandler: function () {
        const id = Data.nextId++;
        Data.handlers[id] = function() {
            console.log("Trying to lock cursor", id);
            const instance = window.unityInstance || window.gameInstance;
            instance.Module.canvas.requestPointerLock();
            Data.pendingCursorLock = 1;
            instance.SendMessage("CursorLockDetector", "HandlePointerLockChange", 1);
            document.removeEventListener("mouseup", Data.handlers[id]);
            delete Data.handlers[id];
        };
        document.addEventListener("mouseup", Data.handlers[id]);
        console.log("Installed cursor lock handler", id);
        return id;
    },

    RemoveCursorLockClickHandler: function (id) {
        // This is identical to fullscreen click handler...
        document.removeEventListener("mouseup", Data.handlers[id]);
        delete Data.handlers[id];
        console.log("removed handler", id);
    },
    
    InstallCursorLockDetector: function () {
        // TODO: make sure we only do this once
        console.log("Installing cursor lock detector");
        const instance = window.unityInstance || window.gameInstance;
        const canvas = instance.Module.canvas;
        document.addEventListener("pointerlockchange", function() {
            var lockElement = document.pointerLockElement;
            const locked = lockElement === canvas;
            console.log("JS cursor lock detector: Pointer lock element changed to", lockElement);
            console.log("JS cursor lock detector: Pointer lock changed, locked:", locked);
            if (!locked && Data.pendingCursorLock) {
                // Is it though?
                console.warn("JS cursor lock detector: Ignoring unlocked event which is likely bogus")
                Data.pendingCursorLock = 0;
                return;
            }
            instance.SendMessage("BetterCursor", "HandlePointerLockChange", locked ? 1 : 0)
            Data.pendingCursorLock = 0;
        });
    },
    
    UnlockCursor: function () {
        console.log("JohanWebGL: calling exitPointerLock");
        document.exitPointerLock();
    },

    InIframe: function () {
        // https://stackoverflow.com/a/326076/652702
        try {
            return window.self !== window.top;
        } catch (e) {
            return true;
        }
    },

    DocumentReferrer: function () {
        const returnStr = document.referrer;

        // https://docs.unity3d.com/Manual/webgl-interactingwithbrowserscripting.html
        const bufferSize = lengthBytesUTF8(returnStr) + 1;
        const buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;
    }
}

autoAddDeps(JohanWebGL, "$Data");
mergeInto(LibraryManager.library, JohanWebGL);
