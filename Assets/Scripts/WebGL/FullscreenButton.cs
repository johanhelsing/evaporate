﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace JGJK
{
    public class FullscreenButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler,
        IPointerUpHandler
    {
#if UNITY_WEBGL
        private int? clickHandler;

        private void EnsureHandler()
        {
            Debug.Log("EnsureHandler");
            if (clickHandler.HasValue)
            {
                return;
            }

            clickHandler = JohanWebGL.AddFullscreenClickHandler();
        }

        private void ClearHandler()
        {
            Debug.Log("ClearHandler");
            if (!clickHandler.HasValue)
            {
                return;
            }

            // The editor doesn't work with jslib
            JohanWebGL.RemoveFullscreenClickHandler(clickHandler.Value);
            clickHandler = null;
        }
#endif // UNITY_WEBGL

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            Debug.Log("Pointer enter");
#if UNITY_WEBGL
            EnsureHandler();
#endif
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            Debug.Log("Pointer exit");
#if UNITY_WEBGL
            ClearHandler();
#endif
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            Debug.Log("Pointer down");
#if UNITY_WEBGL
            EnsureHandler();
#endif
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            Debug.Log("Pointer up");
#if !UNITY_WEBGL
            Screen.fullScreen = true;
#endif
        }
    }
}