using MessagePipe;
using UnityEngine;
using Zenject;

public class Sfx : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [Inject] ISubscriber<ISoundEffect> soundEffects;

    void Start() => soundEffects.Subscribe(Play).AddTo(this);

    public void Play(ISoundEffect soundEffect)
    {
        if (audioSource.isPlaying)
        {
            // Implement audio source pooling if this an issue
            Debug.LogWarning("Audio source already playing, pitch may bork things");
        }
        audioSource.pitch = soundEffect.Pitch;
        audioSource.PlayOneShot(soundEffect.Clip, soundEffect.Volume);
    }
}
