using Cysharp.Threading.Tasks;
using MessagePipe;
using NaughtyAttributes;
using UnityEngine;
using Zenject;

public class PauseMenu : MonoBehaviour
{
    [Required] [SerializeField] CanvasGroup canvasGroup;
    [Required] [SerializeField] CanvasGroupFader fader;

    public void QuitClicked()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    void Start() => canvasGroup.alpha = 1;

    [Inject] void Init(ISubscriber<PauseStateChanged> lockEvents) =>
        lockEvents
            .Subscribe(e => fader.ChangeVisibility(e.Paused))
            .AddTo(this.GetCancellationTokenOnDestroy());
}