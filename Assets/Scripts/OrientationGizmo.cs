using UnityEngine;

public class OrientationGizmo : MonoBehaviour
{
    void OnDrawGizmos()
    {
        var t = transform;
        var p = t.position;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(p, p + t.right);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(p, p + t.up);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(p, p + t.forward);
    }
}
