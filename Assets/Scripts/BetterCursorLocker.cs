using MessagePipe;
using UnityEngine;

namespace JGJK
{
    public interface ICursorLocker
    {
        void TryLock();
        void Unlock();
    }

    public interface ICursorLockState
    {
        bool Locked { get; }
    }

    /// Unity's API is a piece of shit on WebGL, so use this layer of indirection
    public class BetterCursor : MonoBehaviour, ICursorLockState, ICursorLocker
    {
        IPublisher<CursorLockMode> publisher;
        CursorLockMode? currentMode = CursorLockMode.None;
        bool ICursorLockState.Locked => currentMode == CursorLockMode.Locked;

        [Zenject.Inject] public void Init(IPublisher<CursorLockMode> publisher) => this.publisher = publisher;

        void Start()
        {
            // Currently need this for SendMessage to work
            Debug.Assert(name == "BetterCursor");
#if UNITY_WEBGL && !UNITY_EDITOR
            JohanWebGL.InstallCursorLockDetector();
#endif
        }

#if UNITY_WEBGL && !UNITY_EDITOR
        // Unity doesn't properly detect lock state changes on the canvas...
        // so we need to set it manually if we want scripts that check Cursor.lockState to work
        // Called from JS SendMessage
        private void HandlePointerLockChange(int locked)
        {
            var mode = locked != 0 ? CursorLockMode.Locked : CursorLockMode.None;
            UpdateState(mode);
        }
#else
        void Update()
        {
#if UNITY_EDITOR
            // Unity fails to report cursor unlocking in the editor when pressing escape, so we set it manually here
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                UnityEngine.Cursor.lockState = CursorLockMode.None;
            }
#endif
            // For lack of a better Unity API, we're just constantly polling
            UpdateState(UnityEngine.Cursor.lockState);
        }
#endif

        void UpdateState(CursorLockMode mode)
        {
            if (mode == currentMode) return; // Protect from polling
            currentMode = mode;

            Debug.Log($"{nameof(BetterCursor)}: Lock state changed to {currentMode}");
            publisher.Publish(currentMode.Value);
        }

        void ICursorLocker.Unlock()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            JohanWebGL.UnlockCursor();
#else
            UnityEngine.Cursor.lockState = CursorLockMode.None;
#endif
            // UpdateState(CursorLockMode.None);
        }

        void ICursorLocker.TryLock()
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            Debug.LogWarning("Locking cursor from C# code on Unity, expect failure");
            if (!((ICursorLockState) this).Locked && Cursor.lockState == CursorLockMode.Locked) // Unity thinks we're locked, but we're not
            {
                Debug.LogWarning("Unity is confused about lock state, clearing lock before trying to lock again");
                Cursor.lockState = CursorLockMode.None;
            }
#endif
            UnityEngine.Cursor.lockState = CursorLockMode.Locked;
        }
    }

}
