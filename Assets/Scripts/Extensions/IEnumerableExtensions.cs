﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JGJK
{
    public static class IEnumerableExtensions
    {
        public static T ElementAtWrapped<T>(this IEnumerable<T> collection, int index)
        {
            // TODO: can probably be written more efficient (without Count)
            return collection.ElementAt(mod(index, collection.Count()));
        }

        public static T ElementAtOrDefaultWrapped<T>(this IEnumerable<T> collection, int index)
        {
            // TODO: can probably be written more efficient (without Count)
            return collection.ElementAtOrDefault(mod(index, collection.Count()));
        }

        public static IEnumerable<T> ForEachWrap<T>(this IEnumerable<T> collection, Action<T, T, T> lambda)
        {
            var count = collection.Count();
            if (count <= 0) return collection;

            var prev = collection.Last();
            using (var enumerator = collection.GetEnumerator())
            {
                enumerator.MoveNext();
                var current = enumerator.Current;
                var next = prev;
                while (enumerator.MoveNext())
                {
                    next = enumerator.Current;
                    lambda(current, next, prev);
                    prev = current;
                    current = next;
                }

                lambda(current, collection.First(), prev);
            }

            return collection;
        }

        public static T Random<T>(this IEnumerable<T> collection)
        {
            var index = UnityEngine.Random.Range(0, collection.Count());
            return collection.ElementAtOrDefault(index);
        }

        private static int mod(int x, int m)
        {
            var r = x % m;
            return r < 0 ? r + m : r;
        }

        public static TSource? MinByOrNone<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> selector)
            where TSource : struct
        {
            return source.MinByOrNone(selector, null);
        }

        public static TSource? MinByOrNone<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> selector, IComparer<TKey> comparer)
            where TSource : struct
        {
            if (source == null) throw new ArgumentNullException("source");
            if (selector == null) throw new ArgumentNullException("selector");
            comparer = comparer ?? Comparer<TKey>.Default;

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    return default;
                }
                var min = sourceIterator.Current;
                var minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }
                return min;
            }
        }

        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> selector)
        {
            return source.MinBy(selector, null);
        }

        // https://stackoverflow.com/a/914198/652702
        public static TSource MinBy<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> selector, IComparer<TKey> comparer)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (selector == null) throw new ArgumentNullException("selector");
            comparer = comparer ?? Comparer<TKey>.Default;

            using (var sourceIterator = source.GetEnumerator())
            {
                if (!sourceIterator.MoveNext())
                {
                    throw new InvalidOperationException("Sequence contains no elements");
                }
                var min = sourceIterator.Current;
                var minKey = selector(min);
                while (sourceIterator.MoveNext())
                {
                    var candidate = sourceIterator.Current;
                    var candidateProjected = selector(candidate);
                    if (comparer.Compare(candidateProjected, minKey) < 0)
                    {
                        min = candidate;
                        minKey = candidateProjected;
                    }
                }
                return min;
            }
        }

        public static ((TSource item, float score) best, (TSource item, float score) secondBest) Min2ItemsWithScore<TSource>(this IEnumerable<TSource> source, Func<TSource, float> scoreSelector)
        {
            var scores = source.Aggregate(default(((TSource item, float score)?, (TSource item, float score)?)), (acc, nextItem) =>
            {
                var (best, secondBest) = acc;
                var score = scoreSelector(nextItem);
                var next = (nextItem, score);
                if (best == null || best.Value.score > score)
                {
                    return (next, best);
                }

                if (secondBest == null || secondBest.Value.score > score)
                {
                    return (best, next);
                }

                return (best, secondBest);
            });
            var (bestWithScore, secondBestWithScore) = scores;
            if (bestWithScore == null || secondBestWithScore == null)
            {
                throw new InvalidOperationException("Sequence doesn't contain enough elements");
            }

            return (bestWithScore.Value, secondBestWithScore.Value);
        }

        public static (TSource first, TSource second)
            Min2Items<TSource>(this IEnumerable<TSource> source, Func<TSource, float> scoreSelector)
        {
            var (best, secondBest) = source.Min2ItemsWithScore(scoreSelector);
            return (best.item, secondBest.item);
        }

        public static IEnumerable<TComponent> GetComponent<TComponent>(this IEnumerable<Component> collection)
        {
            return collection.Select(i => i.GetComponent<TComponent>()).Where(c => c != null);
        }

        public static IEnumerable<TComponent> GetComponentInParent<TComponent>(this IEnumerable<Component> collection)
        {
            return collection.Select(i => i.GetComponentInParent<TComponent>()).Where(c => c != null);
        }

        public static T TakeFirstOrDefault<T>(this List<T> list)
        {
            if (list.Count == 0) return default;

            var item = list.First();
            list.RemoveAt(0);
            return item;
        }
    }
}