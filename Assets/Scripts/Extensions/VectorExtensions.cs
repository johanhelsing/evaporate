using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

namespace JGJK
{
    public static class Vector3Extensions
    {
        /// <summary>Component-wise division</summary>
        public static Vector3 Divided(this Vector3 dividend, Vector3 divisor)
            => new Vector3(dividend.x / divisor.x, dividend.y / divisor.y, dividend.z / divisor.z);

        public static Vector2 xz(this Vector3 v) => new Vector2(v.x, v.z);
        public static Vector2 xy(this Vector3 v) => new Vector2(v.x, v.y);

        public static Vector3 _yz(this Vector3 v, float x = 0) => new Vector3(x, v.y, v.z);
        public static Vector3 x_z(this Vector3 v, float y = 0) => new Vector3(v.x, y, v.z);
        public static Vector3 xy_(this Vector3 v, float z = 0) => new Vector3(v.x, v.y, z);

        public static Vector3 _y_(this Vector3 v, float x = 0, float z = 0) => new Vector3(x, v.y, z);
        public static Vector3 x__(this Vector3 v, float y = 0, float z = 0) => new Vector3(v.x, y, z);
        public static Vector3 __z(this Vector3 v, float x = 0, float y = 0) => new Vector3(x, y, v.z);

        // Not sure why these aren't already extensions...
        public static Vector3Int RoundToInt(this Vector3 v) => Vector3Int.RoundToInt(v);
        public static Vector3Int FloorToInt(this Vector3 v) => Vector3Int.FloorToInt(v);
        public static Vector3Int CeilToInt(this Vector3 v) => Vector3Int.CeilToInt(v);
    }

    public static class Vector2Extensions
    {
        public static Vector2 FromRadians(float radians)
            => new Vector2(Mathf.Cos(radians), Mathf.Sin(radians));

        public static Vector2 FromDegrees(float degrees)
            => FromRadians(degrees * Mathf.Deg2Rad);

        public static float Degrees(this Vector2 v)
            => Vector2.SignedAngle(Vector2.right, v);

        public static float Radians(this Vector2 v)
            => Vector2.SignedAngle(Vector2.right, v) * Mathf.Deg2Rad;

        public static Vector2 RotatedRadians(this Vector2 v, float radians)
        {
            var sin = Mathf.Sin(radians);
            var cos = Mathf.Cos(radians);
            var tx = v.x;
            var ty = v.y;
            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }

        public static Vector2 Rotated(this Vector2 v, float degrees)
        {
            var radians = degrees * Mathf.Deg2Rad;
            var sin = Mathf.Sin(radians);
            var cos = Mathf.Cos(radians);
            var tx = v.x;
            var ty = v.y;
            return new Vector2(cos * tx - sin * ty, sin * tx + cos * ty);
        }

        public static Vector2 Sign(this Vector2 v) => v.MapComponents(Mathf.Sign);

        public static Vector2 Abs(this Vector2 v) => v.MapComponents(Mathf.Abs);

        public static Vector2 Power(this Vector2 v, float power) =>
            v.MapComponents(c => Mathf.Sign(c) * Mathf.Pow(Mathf.Abs(c), power));

        public static Vector2 Squared(this Vector2 v) => v * v.Abs();

        public static Vector2 Clamped(this Vector2 v, float max) => Vector2.ClampMagnitude(v, max);

        public static Vector2 MapComponents(this Vector2 v, Func<float, float> transformFunction)
            => new Vector2(transformFunction(v.x), transformFunction(v.y));

        public static Vector2 Sum(this IEnumerable<Vector2> source)
            => source.DefaultIfEmpty().Aggregate((x, y) => x + y);

        public static Vector2 Sum<T>(this IEnumerable<T> source, Func<T, Vector2> selector)
            => source.Select(selector).Aggregate((x, y) => x + y);

        public static float Cross(Vector2 a, Vector2 b)
            => a.x * b.y - a.y * b.x;

        public static Vector2 NormalizedOr(this Vector2 v, Vector2 fallback)
            => v == Vector2.zero ? fallback : v.normalized;

        public static Vector3 xy_(this Vector2 v) => new Vector3(v.x, v.y, 0);
        public static Vector3 x_y(this Vector2 v) => new Vector3(v.x, 0, v.y);
        public static Vector3 _xy(this Vector2 v) => new Vector3(0, v.x, v.y);
        public static Vector3 yx_(this Vector2 v) => new Vector3(v.y, v.x, 0);
        public static Vector3 y_x(this Vector2 v) => new Vector3(v.y, 0, v.x);
        public static Vector3 _yx(this Vector2 v) => new Vector3(0, v.y, v.x);

        public static Vector3 xy_(this Vector2 v, float z) => new Vector3(v.x, v.y, z);
        public static Vector3 x_y(this Vector2 v, float z) => new Vector3(v.x, z, v.y);
        public static Vector3 _xy(this Vector2 v, float z) => new Vector3(z, v.x, v.y);
        public static Vector3 yx_(this Vector2 v, float z) => new Vector3(v.y, v.x, z);
        public static Vector3 y_x(this Vector2 v, float z) => new Vector3(v.y, z, v.x);
        public static Vector3 _yx(this Vector2 v, float z) => new Vector3(z, v.y, v.x);

        public static IEnumerable<Vector2> SprayRays(this Vector2 direction, float fieldOfView, float distance, float raySpacingAtDistance = 0.2f)
        {
            var fovRadians = Mathf.Deg2Rad * fieldOfView;
            var step = raySpacingAtDistance / distance;
            var remainder = math.fmod(fovRadians, step);
            yield return direction.RotatedRadians(-fovRadians / 2);
            for (var angle = -fovRadians / 2 + remainder / 2; angle <= fovRadians / 2; angle += step)
            {
                var sprayDirection = direction.RotatedRadians(angle);
                yield return sprayDirection;
            }

            yield return direction.RotatedRadians(fovRadians / 2);
        }

        public static Vector2 NearestAxisAlignedDirection(this Vector2 direction)
        {
            if (direction == Vector2.zero) return Vector2.zero;
            var rawAngle = Vector2.SignedAngle(Vector2.right, direction);
            var angle = Mathf.Round(rawAngle / 90) * 90;
            return FromDegrees(angle);
        }

        public static float ManhattanDistance(Vector2 a, Vector2 b)
            => Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y + b.y);

        public static Vector2 BiLerp(Vector2 x0y0, Vector2 x1y0, Vector2 x0y1, Vector2 x1y1, Vector2 frac)
        {
            var y0 = Vector2.Lerp(x0y0, x1y0, frac.x);
            var y1 = Vector2.Lerp(x0y1, x1y1, frac.x);
            return Vector2.Lerp(y0, y1, frac.y);
        }


        public static Vector2Int RoundToInt(this Vector2 v) => Vector2Int.RoundToInt(v);
        public static Vector2Int FloorToInt(this Vector2 v) => Vector2Int.FloorToInt(v);
        public static Vector2Int CeilToInt(this Vector2 v) => Vector2Int.CeilToInt(v);
    }
}