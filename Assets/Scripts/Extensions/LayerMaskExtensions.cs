﻿using UnityEngine;

namespace JGJK
{
    public static class LayerMaskExtensions
    {
        public static bool Contains(this LayerMask mask, int layer)
            => mask == (mask | (1 << layer));
    }
}