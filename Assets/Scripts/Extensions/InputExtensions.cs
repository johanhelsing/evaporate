using UnityEngine;
using Unity.Mathematics;
#if ENABLE_INPUT_SYSTEM
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
#endif

namespace JGJK
{
    public static class InputExtensions
    {
        public static Vector2 Direction()
#if ENABLE_INPUT_SYSTEM
            => (float2) KeyboardDirection();
#elif ENABLE_LEGACY_INPUT_MANAGER
            => Direction("Horiontal", "Vertical");
#endif

#if ENABLE_INPUT_SYSTEM
        // Warning: Unfortunately keyboard isPressed is buggy on the new input system...
        // keys tend to get stuck...
        public static int KeyboardButtonAxis(KeyControl negative, KeyControl positive)
            => (negative.isPressed ? -1 : 0) + (positive.isPressed ? 1 : 0);

        public static int KeyboardHorizontal()
            => math.clamp(
                KeyboardButtonAxis(Keyboard.current.leftArrowKey, Keyboard.current.rightArrowKey)
                + KeyboardButtonAxis(Keyboard.current.aKey, Keyboard.current.dKey),
                -1, 1);

        public static int KeyboardVertical()
            => math.clamp(
                KeyboardButtonAxis(Keyboard.current.downArrowKey, Keyboard.current.upArrowKey)
                + KeyboardButtonAxis(Keyboard.current.sKey, Keyboard.current.wKey),
                -1, 1);

        public static int2 KeyboardDirection() => new int2(KeyboardHorizontal(), KeyboardVertical());
#endif

#if ENABLE_LEGACY_INPUT_MANAGER
        public static Vector2 Direction(string horizontal, string vertical)
            => new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
#endif

        public static bool GetLeftButtonDown()
            => Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A);

        public static bool GetRightButtonDown()
            => Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D);

        public static bool GetUpButtonDown()
            => Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W);

        public static bool GetDownButtonDown()
            => Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S);

        public static int HorizontalDirectionPressed()
            => (GetLeftButtonDown() ? -1 : 0) + (GetRightButtonDown() ? 1 : 0);

        public static int VerticalDirectionPressed()
            => (GetDownButtonDown() ? -1 : 0) + (GetUpButtonDown() ? 1 : 0);

        public static Vector2Int DirectionPressed()
            => new Vector2Int(HorizontalDirectionPressed(), VerticalDirectionPressed());
    }
}
