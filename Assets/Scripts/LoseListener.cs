using System.Collections;
using System.Collections.Generic;
using MessagePipe;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class LoseListener : MonoBehaviour
{
    [SerializeField] UnityEvent lost;

    [Inject]
    void Init(ISubscriber<LoseEvent> loseEvents)
    {
        loseEvents
            .Subscribe(_ => lost?.Invoke())
            .AddTo(this);
    }
}
