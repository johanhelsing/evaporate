using UnityEngine;

[RequireComponent(typeof(Animation))]
public class PlayAnimationHelper : MonoBehaviour
{
    new Animation animation;
    void Awake() => animation = GetComponent<Animation>();
    public void Play() => animation.Play();
}
