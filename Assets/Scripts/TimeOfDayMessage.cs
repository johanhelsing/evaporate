﻿using MessagePipe;
using UnityEngine;
using Zenject;

public class TimeOfDayMessage : MonoBehaviour
{
    [Inject] GameTime gameTime;
    [Inject] IPublisher<DialogMessage> dialogQueue;

    [SerializeField] float timeOfDay;
    [SerializeField] DialogMessage message;

    void Update()
    {
        if (gameTime.TimeOfDay > timeOfDay)
        {
            dialogQueue.Publish(message);
            Destroy(this);
        }
    }
}