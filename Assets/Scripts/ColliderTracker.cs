using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace JGJK
{
    public class ColliderTracker : MonoBehaviour
    {
        [SerializeField] LayerMask mask;

        public IEnumerable<Collider> Colliders => colliders;
        [ShowNativeProperty]
        public int Count => colliders.Count;

        readonly List<Collider> colliders = new List<Collider>();

        void OnTriggerEnter(Collider other)
        {
            if (mask.Contains(other.gameObject.layer) && !colliders.Contains(other))
            {
                colliders.Add(other);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (colliders.Contains(other))
            {
                colliders.Remove(other);
            }
        }
    }
}
