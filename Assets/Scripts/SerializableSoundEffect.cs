﻿using System;
using UnityEngine;

[Serializable]
public class SerializableSoundEffect : ISoundEffect
{
    [SerializeField] AudioClip clip;
    [SerializeField] float volume = 1;
    [SerializeField] float pitch = 1;

    public SerializableSoundEffect()
    {
    }

    public SerializableSoundEffect(AudioClip clip, float volume = 1, float pitch = 1)
    {
        this.clip = clip;
        this.volume = volume;
        this.pitch = pitch;
    }

    public float Volume => volume;
    public float Pitch => pitch;
    public AudioClip Clip => clip;
}