﻿using JGJK;
using UnityEngine;
using Zenject;

public class CursorLockToggler : MonoBehaviour
{
    [Inject] ICursorLockState cursorState;
    [Inject] ICursorLocker cursorLocker;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (cursorState.Locked)
            {
                cursorLocker.Unlock();
            }
            // The editor already unlocks on escape
            else if (!Application.isEditor)
            {
                Debug.Log("Trying to lock cursor on escape, may fail on WebGL");
                cursorLocker.TryLock();
            }
        }
    }
}