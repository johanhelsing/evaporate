using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace JGJK
{
    public class CursorLockButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler,
        IPointerUpHandler
    {
        [Inject] private ICursorLocker cursorLocker;
#if UNITY_WEBGL
        private int? clickHandler;

        private void EnsureHandler()
        {
            Debug.Log("EnsureHandler");
            if (clickHandler.HasValue)
            {
                return;
            }

            clickHandler = JohanWebGL.AddCursorLockClickHandler();
        }

        private void ClearHandler()
        {
            Debug.Log("ClearHandler");
            if (!clickHandler.HasValue)
            {
                return;
            }

            // The editor doesn't work with jslib
            JohanWebGL.RemoveCursorLockClickHandler(clickHandler.Value);
            clickHandler = null;
        }
#endif // UNITY_WEBGL

        void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
        {
            // Debug.Log("Cursor lock: Pointer enter");
#if UNITY_WEBGL
            EnsureHandler();
#endif
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            // Debug.Log("Cursor lock: Pointer exit");
#if UNITY_WEBGL
            ClearHandler();
#endif
        }

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            // Debug.Log("Cursor lock: Pointer down");
#if UNITY_WEBGL
            EnsureHandler();
#endif
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            // Debug.Log("Cursor lock: Pointer up");
#if !UNITY_WEBGL || UNITY_EDITOR
            // Debug.Log("Locking cursor from non-webgl path");
            cursorLocker.TryLock();
#endif
        }
    }
}
