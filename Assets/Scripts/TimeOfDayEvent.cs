using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class TimeOfDayEvent : MonoBehaviour
{
    [Inject] GameTime gameTime;

    [SerializeField] float timeOfDay;
    [SerializeField] UnityEvent triggered;

    void Update()
    {
        if (gameTime.TimeOfDay > timeOfDay)
        {
            triggered?.Invoke();
            Destroy(this);
        }
    }
}