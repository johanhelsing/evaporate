using NaughtyAttributes;
using UnityEngine;
using Zenject;

public class Sun : MonoBehaviour
{
    [Required] [SerializeField] HeliocentricSunController heliocentricController;
    [Inject] GameTime gameTime;

    public Vector3 Direction => transform.forward;

    void Update() => heliocentricController.TimeOfDay = gameTime.TimeOfDay;
}
