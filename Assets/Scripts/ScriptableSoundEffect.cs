﻿using UnityEngine;

[CreateAssetMenu(fileName = "sfx", menuName = "Sound effect")]
public class ScriptableSoundEffect : ScriptableObject, ISoundEffect
{
    [SerializeField] SerializableSoundEffect effect;
    public float Volume => effect.Volume;
    public float Pitch => effect.Pitch;
    public AudioClip Clip => effect.Clip;
}