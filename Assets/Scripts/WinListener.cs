using System.Collections;
using System.Collections.Generic;
using MessagePipe;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class WinListener : MonoBehaviour
{
    [SerializeField] UnityEvent won;

    [Inject]
    void Init(ISubscriber<WinEvent> winEvents)
    {
        winEvents
            .Subscribe(_ => won?.Invoke())
            .AddTo(this);
    }
}
