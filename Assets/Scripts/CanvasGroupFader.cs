using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using ElRaccoone.Tweens;
using NaughtyAttributes;
using UnityEngine;

public class CanvasGroupFader : MonoBehaviour
{
    [SerializeField] float duration = 0.5f;

    [Button]
    public void Show()
    {
        this.TweenCanvasGroupAlpha(1, duration).SetUseUnscaledTime(true);
    }

    [Button]
    public void Hide()
    {
        this.TweenCanvasGroupAlpha(0, duration).SetUseUnscaledTime(true);
    }

    public void ChangeVisibility(bool shown)
    {
        if (shown) Show();
        else Hide();
    }
}
