﻿using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    [SerializeField] string verb;
    [SerializeField] UnityEvent interacted;

    public UnityEvent Interacted => interacted;
    public string Verb
    {
        get => verb;
        set => verb = value;
    }

    public void Interact()
    {
        interacted?.Invoke();
    }
}