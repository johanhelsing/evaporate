using MessagePipe;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using Zenject;

public class DialogBox : MonoBehaviour
{
    [Required] [SerializeField] TextMeshPro text;
    [Required] [SerializeField] GameObject box;
    [Required] [SerializeField] ScriptableSoundEffect showSound;
    [Inject] IPublisher<ISoundEffect> sfx;

    bool Shown => box.activeSelf;

    void Start() => Hide();

    public void Show(string message)
    {
        // always update text and time
        text.text = message;
        sfx.Publish(showSound);

        // we were already shown skip the rest
        if (Shown) return;
        box.SetActive(true);
    }

    [Button] void ShowHello() => Show("Hello!");

    [Button] public void Hide()
    {
        if (!Shown) return;
        text.text = "";
        box.SetActive(false);
    }
}
