using System.Linq;
using JGJK;
using MessagePipe;
using NaughtyAttributes;
using UnityEngine;
using Zenject;

public class InteractionController : MonoBehaviour
{
    [Required] [SerializeField] Transform cam;
    [Required] [SerializeField] SphereCollider interactionZone;
    [Required] [SerializeField] Transform carryTransform;
    [SerializeField] LayerMask mask;

    [Inject] IPublisher<Interactable> interactableObjectPublisher;

    Interactable current;
    Movable carriedObject;

    void Update()
    {
        if (carriedObject != null)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Debug.Log($"Putting down object {carriedObject}");
                carriedObject.transform.parent = null;
                carriedObject = null;
            }
        }
        else
        {
            var camPos = cam.transform.position;
            var interactable = Physics.OverlapSphere(interactionZone.transform.position, interactionZone.radius, mask)
                .OrderBy(c => (c.ClosestPoint(camPos) - camPos).sqrMagnitude)
                .GetComponentInParent<Interactable>()
                .FirstOrDefault();

            if (current != interactable)
            {
                current = interactable;
                interactableObjectPublisher.Publish(current);
            }

            if (current != null && Input.GetButtonDown("Fire1"))
            {
                current.Interact();
                var movable = current.GetComponent<Movable>();
                if (movable != null)
                {
                    PickUp(movable);
                }
            }
        }
    }

    void PickUp(Movable movable)
    {
        movable.transform.parent = carryTransform;
        interactableObjectPublisher.Publish(null);
        current = null;
        carriedObject = movable;
    }
}