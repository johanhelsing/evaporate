﻿using UnityEngine;

public interface ISoundEffect
{
    public float Volume { get; }
    public float Pitch { get; }
    public AudioClip Clip { get; }
}

public static class SoundEffectExtensions
{
    public static ISoundEffect Copy(this ISoundEffect effect, float pitch)
        => new SerializableSoundEffect(effect.Clip, effect.Volume, effect.Pitch);

    public static ISoundEffect WithPitch(this ISoundEffect effect, float pitch)
        => new SerializableSoundEffect(effect.Clip, effect.Volume, pitch);

    public static ISoundEffect WithPitchVariation(this ISoundEffect effect, float maxVariation = 0.1f)
    {
        var pitch = effect.Pitch * (1 + Random.Range(-maxVariation, maxVariation));
        return new SerializableSoundEffect(effect.Clip, effect.Volume, pitch);
    }

    public static ISoundEffect WithVolume(this ISoundEffect effect, float volume)
        => new SerializableSoundEffect(effect.Clip, volume, effect.Pitch);
}

