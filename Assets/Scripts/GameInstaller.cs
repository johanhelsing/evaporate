using System;
using JGJK;
using MessagePipe;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] GameTime gameTime;
    [SerializeField] Sun sun;
    [SerializeField] GameControl gameControl;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<BetterCursor>().FromNewComponentOnNewGameObject().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<GameTime>().FromInstance(gameTime);
        Container.BindInterfacesAndSelfTo<Sun>().FromInstance(sun);
        Container.BindInterfacesAndSelfTo<GameControl>().FromInstance(gameControl);

        Container.BindInterfacesAndSelfTo<CursorLockPauser>().AsSingle().NonLazy();

        var pipeOptions = Container.BindMessagePipe();
        Container.BindMessageBroker<CursorLockMode>(pipeOptions);
        Container.BindMessageBroker<PauseStateChanged>(pipeOptions);
        Container.BindMessageBroker<WinEvent>(pipeOptions);
        Container.BindMessageBroker<LoseEvent>(pipeOptions);
        Container.BindMessageBroker<Interactable>(pipeOptions);
        Container.BindMessageBroker<DialogMessage>(pipeOptions);
        Container.BindMessageBroker<ISoundEffect>(pipeOptions);

        // set global to enable diagnostics window and global function
        Container.Bind<IServiceProvider>()
            .WithId("ContainerServiceProvider")
            .FromInstance(Container.AsServiceProvider());
    }

    // Doing this in a circumspect way because SetProvider causes a warning
    // when called before container resolving is done
    class GlobalMessagePipeAssigner
    {
        GlobalMessagePipeAssigner([Inject(Id = "ContainerServiceProvider")]
            IServiceProvider serviceProvider)
        {
            GlobalMessagePipe.SetProvider(serviceProvider);
        }
    }
}