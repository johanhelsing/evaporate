using System.Collections.Generic;
using System.Linq;
using NaughtyAttributes;
using UnityEngine;

public class SunExposure : MonoBehaviour
{
    [SerializeField] int sensorsForFullExposure = 1;

    [ShowNativeProperty] int ExposedSensors { get; set; }

    [ShowNativeProperty] public float Exposure => Mathf.Min((float) ExposedSensors / sensorsForFullExposure, 1f);

    readonly List<SunSensor> sensors = new List<SunSensor>();

    void Awake() => sensors.AddRange(GetComponentsInChildren<SunSensor>());
    void Update() => ExposedSensors = sensors.Count(s => !s.Shaded);
}
