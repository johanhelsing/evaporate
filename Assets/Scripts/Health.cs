using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class Health : MonoBehaviour
{
    [Inject] GameControl gameControl;
    [Range(0, 1)] [SerializeField] float health = 1;
    [SerializeField] SunExposure exposure;
    [SerializeField] AnimationCurve exposureToDamage = AnimationCurve.Linear(0, 0, 1, 1);
    [SerializeField] float exposureFactor = 1;
    [SerializeField] UnityEvent died;

    bool dead;

    public float Hp => health;

    void Update()
    {
        if (dead) return;

        var exposureDps = exposureToDamage.Evaluate(exposure.Exposure) * exposureFactor;
        health -= exposureDps * Time.deltaTime;
        if (health < 0)
        {
            dead = true;
            health = 0;
            died?.Invoke();
            gameControl.Lose();
        }
    }

    [Button] void Revive()
    {
        dead = false;
        health = 1;
    }
}
