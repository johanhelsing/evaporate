using NaughtyAttributes;
using Unity.Mathematics;
using UnityEngine;

/// Controller based around real world sun trajectory
public class HeliocentricSunController : MonoBehaviour
{
    // // for debugging
    // [SerializeField] Transform tiltTx, spinTx, locTx;

    [Range(0, 1.5f)] [SerializeField] float _timeOfDay;
    // [Range(0, 1)] [SerializeField] float _timeOfDay;
    [Range(-180, 180)] [SerializeField] float northRotation;
    // For now it's always middle of summer
    [Range(-90, 90)] [SerializeField] float axisTilt = 23.4f; // default to earth
    [Range(-90, 90)] [SerializeField] float latitude = 59.9f; // default to oslo

    [SerializeField] bool animate;
    [ShowIf(nameof(animate))] [SerializeField] float timePerDay = 10;

    public float TimeOfDay
    {
        get => _timeOfDay;
        set
        {
            _timeOfDay = value;
            UpdateDirection();
        }
    }

    void Update()
    {
        if (animate)
        {
            TimeOfDay = math.frac(TimeOfDay + Time.deltaTime / timePerDay);
        }
    }

    void OnValidate() => UpdateDirection();

    void UpdateDirection()
    {
        var timeRotation = TimeOfDay * 360;

        var rot = Quaternion.identity;

        // Start at night
        var spin = -90 + timeRotation;

        // // Just for debug display
        // tiltTx.localRotation = Quaternion.Euler(0, axisTilt, 0);
        // spinTx.localRotation = Quaternion.Euler(spin, 0, 0);
        // locTx.localRotation = Quaternion.Euler(0, 0, latitude);

        rot *= Quaternion.Euler(0, axisTilt, 0);
        rot *= Quaternion.Euler(spin, 0, 0);
        rot *= Quaternion.Euler(0, 0, latitude);

        var sunDirection = Quaternion.Inverse(rot) * Vector3.forward;
        rot = Quaternion.FromToRotation(Vector3.forward, -sunDirection);

        // just so we can tweak it to fit the scene
        rot = Quaternion.Euler(0, northRotation, 0) * rot;

        transform.rotation = rot;
    }
}
