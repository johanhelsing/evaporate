using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using JGJK;
using MessagePipe;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

[Serializable]
public class DialogMessage
{
    [SerializeField] string message;
    [SerializeField] float skipAfter;
    [SerializeField] float autoDismissAfterSeconds;
    [SerializeField] string reply;
    [SerializeField] UnityEvent dismissed;

    public string Message => message;
    public string Reply => reply;

    public float? AutoDismissAfterSeconds => (autoDismissAfterSeconds != 0f) ? (float?)autoDismissAfterSeconds : null;
    public float? SkipAfter => (skipAfter != 0f) ? (float?)skipAfter : null;
    public void Dismiss() => dismissed?.Invoke();
}

public class DialogQueue : MonoBehaviour
{
    [Inject] GameTime gameTime;
    [Inject] GameControl gameControl;

    [SerializeField] List<DialogMessage> queuedMessages;
    [Required] [SerializeField] DialogBox box;
    [Required] [SerializeField] Interactable interactable;

    DialogMessage currentMessage;
    float messageShownAt;

    [Inject]
    public void Init(ISubscriber<DialogMessage> dialogMessages)
    {
        dialogMessages
            .Subscribe(queuedMessages.Add)
            .AddTo(this.GetCancellationTokenOnDestroy());
    }

    public void DismissCurrent()
    {
        box.Hide();
        currentMessage.Dismiss();
        currentMessage = null;
        interactable.gameObject.SetActive(false);
    }

    void Update()
    {
        if (gameControl.Paused) return;

        if (currentMessage != null)
        {
            if (Time.time - messageShownAt > currentMessage.AutoDismissAfterSeconds
                || gameTime.TimeOfDay > currentMessage.SkipAfter)
            {
                DismissCurrent();
            }
        }

        if (currentMessage == null)
        {
            do
            {
                currentMessage = queuedMessages.TakeFirstOrDefault();
            } while (currentMessage != null && gameTime.TimeOfDay > currentMessage.SkipAfter);

            if (currentMessage != null)
            {
                messageShownAt = Time.time;
                box.Show(currentMessage.Message);
                interactable.gameObject.SetActive(true);

                var reply = currentMessage.Reply;
                if (string.IsNullOrEmpty(reply)) reply = "...";
                interactable.Verb = reply;
            }
        }
    }
}
