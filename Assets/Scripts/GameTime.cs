﻿using NaughtyAttributes;
using Unity.Mathematics;
using UnityEngine;

public class GameTime : MonoBehaviour
{
    [SerializeField] float secondsPerDay;
    [SerializeField] float time;
    [SerializeField] bool resetOnStart = true;
    [SerializeField] bool debugFreeze;
    // [ShowNativeProperty] public float TimeOfDay => math.frac(time / secondsPerDay);
    [ShowNativeProperty] public float TimeOfDay => time / secondsPerDay;

    [Button] public void Pause() => Time.timeScale = 0;

    [Button] public void Unpause() => Time.timeScale = 1;

    [Button] void Noon() => time = secondsPerDay / 2;
    [Button] void Morning() => time = secondsPerDay / 3;
    [Button] void Afternoon() => time = secondsPerDay * 0.7f;
    [Button] void Day2() => time = secondsPerDay * 1f;

    void Awake()
    {
        if (resetOnStart)
        {
            time = 0;
        }
        Time.timeScale = 0;
    }

    void Update()
    {
        if (Application.isEditor && Input.GetKeyDown(KeyCode.P))
        {
            Pause();
        }

        if (!debugFreeze)
        {
            time += Time.deltaTime;
        }
    }
}