#ifndef JGJK_SHADER_FUNCTIONS_HLSL
#define JGJK_SHADER_FUNCTIONS_HLSL

#include "common.hlsl"

float sd_rounded_box(float2 p, float2 b, float4 r)
{
    r.xy = p.x > 0.0 ? r.xy : r.zw;
    r.x  = p.y > 0.0 ? r.x : r.y;
    float2 q = abs(p) - b + r.x;
    return min(max(q.x, q.y), 0.0) + length(max(q, 0.0)) - r.x;
}

float sd_rounded_box_uniform(float2 p, float2 b, float r)
{
    float2 q = abs(p) - b + r;
    return min(max(q.x, q.y), 0.0) + length(max(q, 0.0)) - r;
}
DECLARE_2F2F1F_1F(sd_rounded_box_uniform);

#endif