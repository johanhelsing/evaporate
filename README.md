# evaporate

A short first-person game about a snowman in the desert and his fondness of camels

Play: https://jhelsing.itch.io/evaporate

Devlog: https://johanhelsing.studio/posts/evaporate-post-mortem
